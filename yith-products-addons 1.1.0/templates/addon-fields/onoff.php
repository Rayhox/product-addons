<?php
/** Addon frontend onoff template */

?>
<label class="yith-pa-addon__desciption" for="yith-pa-<?php echo esc_html( $product_id ) . '-' . esc_html( $index ); ?>">
	<?php echo isset( $description ) ? esc_html( $description ) : ''; ?>
</label>
<span class="yith-pa-onoof-container yith-pa-addon__input"> 
	<label class="yith-pa-switch-frontend">		
		<input type="checkbox" class="yith-pa-addon__input yith-pa-field__onoff yith-proteo-standard-checkbox"
			id="yith-pa-<?php echo esc_html( $product_id ) . '-' . esc_html( $index ); ?>"
			name="yith-pa-field-<?php echo intval( $index ); ?>"
			value="yes">
		<span class="slider round"></span>
	</label>
	<label for="yith-pa-onoff-<?php echo intval( $index ); ?>"
		class="yith-pa-onoff__button <?php echo isset( $def_ena ) && 'yes' === $def_ena ? 'yith-pa-onoff__button--checked' : ''; ?>">
	</label>
</span>
