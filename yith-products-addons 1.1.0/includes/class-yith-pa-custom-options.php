<?php
/**
 * This file belongs to the YITH Products Addons.
 *
 * This source file is subject to the GNU GENERAL PUBLIC LICENSE (GPL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.gnu.org/licenses/gpl-3.0.txt
 */

if ( ! defined( 'YITH_PA_VERSION' ) ) {
	exit( 'Direct access forbidden.' );
}

if ( ! class_exists( 'YITH_PA_Cst_Opt' ) ) {

	/**
	 * YITH_PA_Cst_Opt
	 */
	class YITH_PA_Cst_Opt {

		/**
		 * Main Instance
		 *
		 * @var YITH_PA_Cst_Opt
		 * @since 1.0
		 * @access private
		 */

		private static $instance;

		/**
		 * Main plugin Instance
		 *
		 * @return YITH_PA_Cst_Opt Main instance
		 * @author Héctor García
		 */
		public static function get_instance() {
			return ! is_null( self::$instance ) ? self::$instance : self::$instance = new self();
		}

		/**
		 * YITH_PA_Frotend constructor.
		 */
		private function __construct() {
			// Creates the custom tab products options.
			add_filter( 'woocommerce_product_data_tabs', array( $this, 'yith_pa_add_options' ) );
			add_action( 'woocommerce_product_data_panels', array( $this, 'initialize_pa_panel_fields' ) );
			// Saves the addon data.
			add_action( 'woocommerce_admin_process_product_object', array( $this, 'save_addons' ) );
			// Variations.
			add_action( 'woocommerce_product_after_variable_attributes', array( $this, 'yith_add_addons_in_variations' ), 10, 3 );
			add_action( 'woocommerce_admin_process_variation_object', array( $this, 'yith_save_variations_addons' ), 10, 2 );
		}

		/**
		 * Creates the new custom tab
		 *
		 * @param mixed $tabs Default WordPress tabs.
		 * @return $tabs
		 */
		public function yith_pa_add_options( $tabs ) {
			$tabs['yith-pa-tab'] = array(
				'label'    => 'Add-ons',
				'target'   => 'yith_pa_tab',
				'class'    => array(),
				'priority' => 35,
			);
			return $tabs;
		}
		/**
		 * Initialize input types
		 *
		 * @return void
		 */
		public function initialize_pa_panel_fields() {
			echo '<div id="yith_pa_tab" class="panel woocommerce_options_panel hidden">';
			$this->yith_pa_show_options();
			echo '</div>';

		}
		/**
		 * Get the view to display on the options
		 *
		 * @return void
		 */
		public function yith_pa_show_options() {
			global $post;
			$product = wc_get_product( $post->ID );
			$addons  = $product->get_meta( 'yith_pa-addon' );
			$addons  = is_array( $addons ) ? $addons : array();

			echo '<div class="yith-pa-add-ons ui-sortable">';
			yith_pa_get_view( '/addons-container.php', compact( 'addons' ) );
			echo '</div>';

		}

		/**
		 * Save the addons
		 *
		 * @param  mixed $product WC product.
		 * @return void
		 */
		public function save_addons( $product ) {
			if ( ! isset( $_POST['yith_pa-addon'][0] ) ) {
				return;
			}
			foreach ( $_POST['yith_pa-addon'][0] as $key => $addon ) {
				$addons[ $key ] = $addon;
			}
			if ( array_key_exists( '{{INDEX}}', $addons ) ) {
				unset( $addons['{{INDEX}}'] );
			}
			$this->sort_addons_options( $addons );

			array_walk( $addons, array( $this, 'sanitize_text_array' ) );
			uasort( $addons, array( $this, 'sort_by_index' ) );
			$product->update_meta_data( 'yith_pa-addon', $addons );

		}

		/**
		 * Return true if the index value of the first array is higher than the second one
		 *
		 * @param  mixed $x First array to check.
		 * @param  mixed $y Second array to check.
		 */
		public function sort_by_index( $x, $y ) {
			return intval( $x['index'] ) > intval( $y['index'] );
		}

		/**
		 * Sanitize the addon array
		 *
		 * @param  mixed $element Element to modify.
		 * @param  mixed $key     Key element to edit.
		 * @return void
		 */
		public function sanitize_text_array( &$element, $key ) {
			if ( is_array( $element ) ) {
				array_walk( $element, array( $this, 'sanitize_text_array' ) );
			} else {
				switch ( $key ) {
					case 'enabled':
					case 'default_enabled':
						$element = 'yes' === $element ? 'yes' : 'no';
						break;
					case 'index':
					case 'free_char':
						$element = absint( $element );
						break;
					case 'price':
						$element = floatval( str_replace( ',', '.', $element ) );
						$element = intval( $element * 100 ) / 100;
						break;
					default:
						$element = sanitize_text_field( wp_unslash( $element ) );
						break;
				}
			}
		}

		/**
		 * Sort addons options
		 *
		 * @param  mixed $addons Addon array.
		 * @return void
		 */
		public function sort_addons_options( &$addons ) {
			foreach ( $addons as &$addon ) {
				$options = array();
				foreach ( $addon['options']['text'] as $key => $text ) {
					if ( ! ! $text ) {
						$options[] = array(
							'name'  => sanitize_text_field( wp_unslash( $text ) ),
							'price' => $addon['options']['price'][ $key ],
						);
					}
				}
				if ( ! $options ) {
					$options[] = array(
						'name'  => '',
						'price' => '',
					);
				}
				$addon['options'] = $options;
			}
		}

		/**
		 * Show Add-ons on products variations
		 *
		 * @param  mixed $loop           Position in the loop.
		 * @param  mixed $variation_data Variation data.
		 * @param  mixed $variation      Post data.
		 * @return void
		 */
		public function yith_add_addons_in_variations( $loop, $variation_data, $variation ) {

			$product_variation = wc_get_product( $variation );
			$addons            = $product_variation->get_meta( 'yith_pa-addon' );
			$addons            = is_array( $addons ) ? $addons : array();
			yith_pa_get_view( '/addons-container.php', compact( 'addons', 'loop' ) );

		}

		/**
		 * Save the addons on variations products
		 *
		 * @param  mixed $variation_id Post data.
		 * @param  mixed $i            Variation identifier.
		 * @return void
		 */
		public function yith_save_variations_addons( $variation_id, $i ) {
			++$i;
			$variation = wc_get_product( $variation_id );
			if ( ! isset( $_POST['yith_pa-addon'][ $i ] ) ) {
				return;
			}
			foreach ( $_POST['yith_pa-addon'][ $i ] as $key => $addon ) {
				$addons[ $key ] = $addon;
			}
			if ( array_key_exists( '{{INDEX}}', $addons ) ) {
				unset( $addons['{{INDEX}}'] );
			}
			$this->sort_addons_options( $addons );

			array_walk( $addons, array( $this, 'sanitize_text_array' ) );
			uasort( $addons, array( $this, 'sort_by_index' ) );
			$variation->update_meta_data( 'yith_pa-addon', $addons );
			$variation->save();

		}
	}
}
