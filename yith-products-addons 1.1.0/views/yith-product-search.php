<?php

?>

<p class="form-field">
	<label class="yith-search-products-label">Products</label>
	<select class="wc-product-search select2-hidden-accessible enhanced" name="products_ids[]" data-placeholder="Search for a product..."
		data-action="woocommerce_json_search_products_and_variations" tabindex="-1" aria-hidden="true">
	</select>
	<span class="select2 select2-container select2-container--default" dir="ltr">
		<span class="selection">
			<span class="select2-selection select2-selection--multiple" aria-haspopup="true" aria-expanded="false" tabindex="-1">
				<ul class="select2-selection__rendered" aria-live="polite" aria-relevant="additions removals" aria-atomic="true">
					<li class="select2-search select2-search--inline">
						<input class="select2-search__field" type="text" tabindex="0" autocomplete="off" autocorrect="off" autocapitalize="none" spellcheck="false" role="textbox" aria-autocomplete="list" placeholder="Search for a product…" style="width: 467.391px;">
					</li>
				</ul>
			</span>
		</span>
		<span class="dropdown-wrapper" aria-hidden="true"></span>
	</span>
</p>
