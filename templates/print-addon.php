<?php
/**
 * This file belongs to the YITH Products Addons.
 *
 * This source file is subject to the GNU GENERAL PUBLIC LICENSE (GPL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.gnu.org/licenses/gpl-3.0.txt
 */

$decimal_separator = wc_get_price_decimal_separator();

$data = '';
$options_data = json_encode( $addon['options'] );
switch ( $addon['field_type'] ) {
	case 'text':
	case 'textarea':
		$data = 'data-price-settings=' . esc_attr( $addon['price_settings'] ) . ' data-price=' . esc_attr( $addon['price'] ) . '
		data-free-chars=' . intval( $addon['free-char'] ) . '';
		break;
	case 'onoff':
	case 'checkbox':
		if ( 'fixed' === $addon['price_settings'] ) {
			$data = 'data-price=' . esc_attr( $addon['price'] ) . '';
		}
		break;
	case 'select':
	case 'radio':
		$data = 'data-options=' . $options_data . ' data-price-settings=' . $addon['price_settings'] . '';
		break;
}
$class = 'yith-pa-hide';
if ( isset( $addon['def_ena'] ) && 'yes' === $addon['def_ena'] && 'checkbox' === $addon['field_type'] || 'onoff' === $addon['field_type'] ) {
	$class = '';
}
?>

<div class="yith-pa-addon"<?php echo esc_html( $data ); ?>>
	<?php if ( isset( $addon['name'] ) ) : ?>
		<div class="yith-pa-addon__name">
			<label for="yith-pa-<?php echo esc_html( $addon['product_id'] ) . '-' . esc_html( $addon['index'] ); ?>">
			<?php echo esc_html( $addon['name'] ); ?>
			</label>
		</div>
	<?php endif; ?>
	<?php
	wc_get_template( 'addon-fields/' . $addon['field_type'] . '.php', $addon, '', YITH_PA_DIR_TEMPLATES_PATH . '/' );
	?>
	<?php if ( 'free' !== $addon['price_settings'] && ! ! $addon['price'] ) : ?>
		<div class="yith-pa-price__container <?php echo esc_html( $class ); ?>">
			<?php print_price_on_addon( $addon['price'], true ); ?>
		</div>
	<?php endif; ?>
</div>
