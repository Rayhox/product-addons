<?php
/** Addon frontend checkbox template */

?>
<label class="yith-pa-checkbox-container">
	<input type="checkbox" class="yith-pa-addon__input yith-pa-field__checkbox yith-proteo-standard-checkbox"
		id="yith-pa-<?php echo esc_html( $product_id ) . '-' . esc_html( $index ); ?>" <?php checked( isset( $def_ena ) && 'yes' === $def_ena ); ?>
		name="yith-pa-field-<?php echo intval( $index ); ?>"
		value="yes"
		>
	<span class="yith-pa-addon__checkbox-desciption" for="yith-pa-<?php echo esc_html( $product_id ) . '-' . esc_html( $index ); ?>">
		<?php echo isset( $description ) ? esc_html( $description ) : ''; ?>
	</span>
</label>
