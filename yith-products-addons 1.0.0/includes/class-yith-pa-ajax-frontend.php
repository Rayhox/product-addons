<?php
/**
 * This file belongs to the YITH Products Addons.
 *
 * This source file is subject to the GNU GENERAL PUBLIC LICENSE (GPL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.gnu.org/licenses/gpl-3.0.txt
 */

if ( ! defined( 'YITH_PA_VERSION' ) ) {
	exit( 'Direct access forbidden.' );
}

if ( ! class_exists( 'YITH_PA_Ajax_Fnd' ) ) {

	/**
	 * YITH_PA_Ajax_Fnd
	 */
	class YITH_PA_Ajax_Fnd {

		/**
		 * Main Instance
		 *
		 * @var YITH_PA_Ajax_Fnd
		 * @since 1.0
		 * @access private
		 */

		private static $instance;

		/**
		 * Main plugin Instance
		 *
		 * @return YITH_PA_Ajax_Fnd Main instance
		 * @author Héctor García
		 */
		public static function get_instance() {
			return ! is_null( self::$instance ) ? self::$instance : self::$instance = new self();
		}

		/**
		 * YITH_PA_Ajax_Fnd constructor.
		 */
		private function __construct() {

		}


	}
}
