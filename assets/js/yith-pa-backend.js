jQuery(document).ready(function ( $ ) {
    
    //MAKE THE ADDONS SORTABLE
    $( '.yith-pa-addons' ).sortable(
        {
            handle: '.yith-pa-addon-header',
            item: '.yith-pa-addon-container',
            update: function( event, ui ) {
                index = 0;
                $( '.yith-pa-addon-container' ).each(
                    function() {
                        var indexInput = $( this ).find(
                            '.yith-pa-addon-index'
                        );
                        indexInput.attr(
                            'value',
                            index
                        );
                        index++;
                    }
                );
            }
        }
    );
    
    //REMOVES THE OPTION
    removeOption = function(){
        var addonContainer = getAddonContainer( $( this ) ),
            errase         =  $( this ).closest( '.yith-pa-options-container' ),
            erraseButton   = addonContainer.find( '.yith-pa-remove-option' ),
            numOptions     = addonContainer.find( '.yith-pa-options-block div' );
        
        if ( numOptions.length <= 9 ) {
            erraseButton.hide();
        }; 
        
        errase.fadeTo( 600, 0, function() {
                $( this ).remove();
        }).slideUp( {duration: 600, queue: false } );
    };
    //ADD NEW OPTION
    addNewOption = function(){
        var addonContainer        = getAddonContainer( $( this ) ),
            optionContainer       = addonContainer.find( '.yith-pa-options-block' ),
            optionTemplate        = addonContainer.find( '.yith-pa-options-container__template' ),
            optionHtml            = optionTemplate.html(),
            erraseButton          = addonContainer.find( '.yith-pa-remove-option' ),
            numOptions            = addonContainer.find( '.yith-pa-options-block div' );

        optionContainer.append( '<div class="yith-pa-options-container">' + optionHtml + '</div>' );
        if ( numOptions.length >= 3 ) {
            erraseButton.show();
        };
    };
    // WRITES THE NAME ON THE ADD-ON REALTIME
    adddonHeaderName = function(){
        var addonContainer = getAddonContainer( $( this ) ),  
            toWrite        = addonContainer.find( '.yith-pa-header__label' );
        toWrite.text( $( this ).val() );

    };
    // REMOVES THE ADDON WHEN BUTTON IS PRESSED
    removeAddon = function() {
        var addonContainer = getAddonContainer( $( this ) );
        addonContainer.fadeTo( 600, 0, function() {
            $( this ).remove();
        }).slideUp( {duration: 600, queue: false } );
    };
    // DISPLAY THE BODY OF THE TEMPLATE WHEN CLICK ON THE ARROW
    toggleAddon = function() {
        var addonContainer = getAddonContainer( $( this ) ),
            addon          = addonContainer.find( '.yith-pa-addon' ),
            arrowContainer = addonContainer.find( '.yith-pa-addon-header-arrow' );
        
        addon.slideToggle();
        addonContainer.toggleClass( 'yith-pa-total-container__closed' );

        arrowContainer.toggleClass( function() {
            if ( addon.is( ':visible' ) ) {
                return '.dashicons dashicons-arrow-up-alt2';
            } else {
                return '.dashicons dashicons-arrow-down-alt2';
            }

        });
        
    };
    // HIDE OR SHOW ELEMENTES BASED ON FIELD TYPES
    fieldTypeChange = function() {
        var addonContainer = getAddonContainer( $( this ) ),
            price          = addonContainer.find('.yith-pa-price').parents().eq(1),
            free_char      = addonContainer.find('.yith-pa-free-char').parents().eq(1),
            enabl          = addonContainer.find('.yith-pa-def-ena').parents().eq(2),
            options        = addonContainer.find('.yith-pa-select-option__text-input').closest( '.yith-pa-field-container' ),
            optionPrice    = addonContainer.find('.yith-pa-select-option__price-input').parent(),
            price_per_char = addonContainer.find('.yith-pa-radio-price-per-char').parents().eq(1),
            free           = addonContainer.find('.yith-pa-radio-free');
            switch ( $( this ).val() ) {
            case 'text':
                enabl.slideUp('fast');
                options.slideUp('fast');
                price_per_char.slideDown('fast');
                if ( free.prop('checked') ){
                    price.slideUp('fast');
                    free_char.slideUp('fast'); 
                    // if ( options.is(":visible") ) {
                    //     optionPrice.slideUp('fast');
                    // }else{
                    //     optionPrice.slideDown('fast');
                    // }
                }else{
                    price.slideDown('fast');
                    free_char.slideDown('fast');
                }
                break;
            case 'textarea':
                enabl.slideUp('fast');
                options.slideUp('fast');
                price_per_char.slideDown('fast');
                if ( free.prop('checked') ){
                    price.slideUp('fast');
                    free_char.slideUp('fast'); 
                    // if ( options.is(":visible") ) {
                    //     optionPrice.slideUp('fast');
                    // }else{
                    //     optionPrice.slideDown('fast');
                    // }
                }else{
                    price.slideDown('fast');
                    free_char.slideDown('fast');
                }
                break;
            case 'select':
                enabl.slideUp('fast');
                options.slideDown('fast');
                price_per_char.slideUp('fast');
                if ( free.prop('checked') ){
                    price.slideUp('fast');
                    free_char.slideUp('fast');
                    optionPrice.slideUp('fast');
                }else{
                    price.slideUp('fast');
                    free_char.slideUp('fast');
                    optionPrice.slideDown('fast');
                }
                break;
            case 'radio':
                enabl.slideUp('fast');
                options.slideDown('fast');
                price_per_char.slideUp('fast');
                if ( free.prop('checked') ){
                    price.slideUp('fast');
                    free_char.slideUp('fast');
                    optionPrice.slideUp('fast');
                }else{
                    price.slideUp('fast');
                    free_char.slideUp('fast');
                    optionPrice.slideDown('fast');
                }
                break;
            case 'checkbox':
                enabl.slideDown('fast');
                options.slideUp('fast');
                price_per_char.slideUp('fast');
                free_char.slideUp('fast');
                if ( free.prop('checked') ){
                    price.slideUp('fast');
                }else{
                    price.slideDown('fast');
                }
                break;
            case 'onoff':
                enabl.slideDown('fast');
                options.slideUp('fast');
                price_per_char.slideUp('fast');
                free_char.slideUp('fast');
                if ( free.prop('checked') ){
                    price.slideUp('fast');
                }else{
                    price.slideDown('fast');
                }
                break;
        };
    };
    // HIDE OR SHOW ELEMENTES BASED ON PRICE SETTINGS
    priceSettingsChange = function() {
        var addonContainer = getAddonContainer( $( this ) ),
            field_type     = addonContainer.find('.yith-pa-field-type');    
            price          = addonContainer.find('.yith-pa-price').parents().eq(1),
            optionPrice    = addonContainer.find('.yith-pa-select-option__price-input').parent(),
            free_char      = addonContainer.find('.yith-pa-free-char').parents().eq(1),
            options        = addonContainer.find('.yith-pa-select-option__text-input').parents().eq(8);
        switch ( $( this ).val() ) {
            case 'free':
                price.slideUp('fast');
                free_char.slideUp('fast');
                if ( options.is(":visible") ) {
                    optionPrice.slideUp('fast');
                }else{
                    optionPrice.slideDown('fast');
                }
                break;
            case 'fixed':
                switch ( field_type.val() ) {
                    case 'text':
                        price.slideDown('fast');
                        free_char.slideDown('fast');
                        break;
                    case 'textarea':
                        price.slideDown('fast');
                        free_char.slideDown('fast');
                        break;
                    case 'select':
                        price.slideUp('fast');
                        free_char.slideUp('fast');
                        if ( options.is(":visible") ) {
                            optionPrice.slideDown('fast');
                        }
                        break;
                    case 'radio':
                        price.slideUp('fast');
                        free_char.slideUp('fast');
                        if ( options.is(":visible") ) {
                            optionPrice.slideDown('fast');
                        }
                        break;
                    case 'checkbox':
                        price.slideDown('fast');
                        free_char.slideUp('fast');
                        break;
                    case 'onoff':
                        price.slideDown('fast');
                        free_char.slideUp('fast');
                        break;
                }
            case 'price-per-char':
                if ( field_type.val() === 'text' || field_type.val() === 'textarea' ) {
                    price.slideDown('fast');
                    free_char.slideDown('fast');
                }
                break;
        }
    };
    //RETURN THE ADDON CONTAINER OF THE ELEMENT
    getAddonContainer   = function( element ) {
        return element.closest( '.yith-pa-addon-container' );
    };
    

    // DUPLICATES DE HIDDEN TEMPLATE TO SHOW A NEW ONE
    var index = 0;
    addNewAddon = function(){
        var totalContainer  =  $( this ).closest( '.yith-pa-total-container' ),
            addonsContainer = totalContainer.find( '.yith-pa-addons' ),
            addonsTemplate  = totalContainer.find( '.yith-pa-template' ),
            newAddon        = $( addonsTemplate.html().replaceAll( '{{INDEX}}', index) ),
            newAddonIndex   = newAddon.find( '.yith-pa-addon-index' );
        
        newAddonIndex.attr( 'value', index );
        addonsContainer.append( newAddon );
        initAddon( newAddon );
        newAddon.removeClass( 'yith-pa-addon-container__closed' );
        newAddon.find( '.yith-pa-addons' ).show();
        index ++;
    };

    // SHOW DEFAULT OPTIONS WHEN CREATES NEW TEMPLATE
    initAddon = function( add0n ) {

        add0n.find( '.yith-pa-radio__input' ).trigger( 'change' );
        add0n.find( '.yith-pa-field-type' ).trigger( 'change' );
        add0n.find( '.yith-pa-name' ).trigger( 'keyup' );
        add0n.find( '.yith-pa-addon' ).hide();
        // add0n.addClass( 'yith-pa-addon-container__closed' );
        
    };
    // TRIGGERS WHEN THE VARIATIONS ARE LOADING
    variationsLoad = function() {
        $( '.woocommerce_variable_attributes .yith-pa-addons .yith-pa-addon-container' ).each( function() {        
            var addons = $( this ).closest( '.yith-pa-addons' );
            addons.sortable( {      
                handle: '.yith-pa-addon-header',
                item: '.yith-pa-addon-container',
                update: function( event, ui ) {
                    index = 0;
                    $( '.yith-pa-addon-container' ).each(
                        function() {
                            var indexInput = $( this ).find(
                                '.yith-pa-addon-index'
                            );
                            indexInput.attr(
                                'value',
                                index
                            );
                            index++;
                        }
                    );
                }
            } )          
            $( this ).html( $( this ).html().replaceAll( '{{INDEX}}', index++ ) );
            initAddon( $( this ) );
        });
    };

    // LISTENERS

        // CREATES NEW ADD-ON
        $( document ).on( 'click', '.yith-pa-add-new-addon', addNewAddon );
        // TOGGLE ADDON
        $( document ).on( 'click', '.yith-pa-addon-header-arrow', toggleAddon );
        // REMOVE ADDON
        $( document ).on( 'click', '.yith-pa-remove-addon', removeAddon );
        // ADDON NAME
        $( document ).on( 'keyup', '.yith-pa-name', adddonHeaderName );
        // ADD OPTION
        $( document ).on( 'click', '.yith-pa-add-new-option', addNewOption );
        // REMOVE OPTION 
        $( document ).on( 'click', '.yith-pa-remove-option', removeOption );
        //FIEL TYPE CHANGE
        $( document ).on( 'change', '.yith-pa-field-type', fieldTypeChange );
        //PRICE SETTINGS CHANGE
        $( document ).on( 'change', '.yith-pa-radio__input', priceSettingsChange );
        //VARIATIONS LOAD
        $( document ).on( 'woocommerce_variations_loaded', variationsLoad );
           
        
    // TRIGGERS WHEN THE PAGE LOADS
    $( '.yith-pa-addons .yith-pa-addon-container' ).each( function() {
        
        $( this ).html( $( this ).html().replaceAll( '{{INDEX}}', index ) );
        index++;
        $( this ).find( '.yith-pa-radio__input' ).trigger( 'change' );
        //$( this ).find( '.yith-pa-field-type' ).trigger( 'change' );
        $( this ).find( '.yith-pa-name' ).trigger( 'keyup' );
        $( this ).find( '.yith-pa-addon' ).hide();

    });
});