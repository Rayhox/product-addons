<?php

?>
<div class="yith-pa-search yith-pa-product-select" id="yith-pa-search">
	<p class="form-field">
		<label><?php _e( 'Products', 'woocommerce' ); ?></label>
		<select class="wc-product-search" multiple="multiple" style="width: 50%;" id="yith_pa-addon" name="yith_pa-addon[product_ids][]" data-placeholder="<?php esc_attr_e( 'Search for a product&hellip;', 'woocommerce' ); ?>" data-action="woocommerce_json_search_products">
			<?php
			foreach ( $products_id as $product_id ) {
				$product = wc_get_product( $product_id );
				if ( is_object( $product ) ) {
					echo '<option value="' . esc_attr( $product_id ) . '"' . selected( true, true, false ) . '>' . esc_html( wp_strip_all_tags( $product->get_formatted_name() ) ) . '</option>';
				}
			}
			?>
		</select>
	</p>
</div> 
