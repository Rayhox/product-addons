<?php 

?>

<div class="wrap">
	<h1><?php esc_html_e( 'Add-ons options', 'yith-products-addons' ); ?></h1>

	<form method="post" action="options.php">
		<?php
		settings_fields( 'yith-pa-options-page' );

		do_settings_sections( 'yith-pa-options-page' );

		esc_html__( submit_button( 'Save Add-ons' ), 'yith-products-addons' );
		?>

	</form>
</div>
