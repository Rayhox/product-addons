<?php
/** Addon backend template */
$field_types = array( 'text', 'textarea', 'select', 'radio', 'checkbox', 'onoff' );
$loop        = isset( $loop ) ? $loop : 0;
// error_log( print_r( $addon, true ) );
?>
<div class="yith-pa-addon-container" id="yith-pa-addon-container">
	<!-- HEADER -->
	<div class="yith-pa-addon-header">
		<span class="dashicons dashicons-arrow-down-alt2 yith-pa-addon-header-arrow"></span>
		<span class="yith-pa-addon-header-toggle">
			<span class="yith-pa-header__label"></span>
			<label class="yith-pa-switch-total">
				<input type="checkbox" class="yith-pa-addon-header-input-onoff" value='yes' name="yith_pa-addon[<?php echo $loop; ?>][{{INDEX}}][enabled]"
				id="yith-pa-addon-enabled-<?php echo $loop; ?>-{{INDEX}}" <?php checked( isset( $addon[ $loop ]['enabled'] ) && 'yes' === $addon[ $loop ]['enabled'] ); ?>>
				<span class="slider round"></span>
			</label>
		<label for="yith-pa-addon-enabled-<?php echo $loop; ?>-{{INDEX}}" class="yith-pa__toggle-field-input__toggle">
		</label>
		</span>
	</div>
	<div class="yith-pa-addon">
		<!-- BODY -->
		<div class="yith-pa-addon-body">
			<input type="hidden" class="yith-pa-addon-index" name="yith_pa-addon[<?php echo $loop; ?>][{{INDEX}}][index]"
			value="<?php echo intval( isset( $addon[ $loop ]['index'] ) ? $addon[ $loop ]['index'] : -1 ); ?>">

			<!-- NAME FIELD -->
			<div class="yith-pa-field-container">
				<label for="yith-pa-name-<?php echo $loop; ?>-{{INDEX}}" class="yith-pa-field__label">
				<?php esc_html_e( 'Name', 'yith-products-addons' ); ?></label>
				<div class="yith-pa-input-container">
					<input type="text" id="yith-pa-name-<?php echo $loop; ?>-{{INDEX}}" class="yith-pa-field__input yith-pa-name"
					name="yith_pa-addon[<?php echo $loop; ?>][{{INDEX}}][name]"
					value="<?php echo esc_html( isset( $addon[ $loop ]['name'] ) ? $addon[ $loop ]['name'] : '' ); ?>">
					<div class="yith-pa-input-desc"><?php esc_html_e( 'The name of the add-on', 'yith-products-addons' ); ?></div>
				</div>
			</div>
			<!-- DESCRIPTION FIELD -->
			<div class="yith-pa-field-container">
				<label for="yith-pa-desc-<?php echo $loop; ?>-{{INDEX}}" class="yith-pa-field__label">
				<?php esc_html_e( 'Description', 'yith-products-addons' ); ?></label>
				<div class="yith-pa-input-container">
					<textarea id="yith-pa-desc-<?php echo $loop; ?>-{{INDEX}}" class="yith-pa-field__input"
					name="yith_pa-addon[<?php echo $loop; ?>][{{INDEX}}][description]"><?php echo esc_html( isset( $addon[ $loop ]['description'] ) ? $addon[ $loop ]['description'] : '' ); ?></textarea>
					<div class="yith-pa-input-desc"><?php esc_html_e( 'Set here the description that will be shown above the add-on field', 'yith-products-addons' ); ?></div>
				</div>
			</div>
			<!-- SELECT FIELD -->
			<div class="yith-pa-field-container">
				<label for="yith-pa-select-<?php echo $loop; ?>-{{INDEX}}" class="yith-pa-field__label">
				<?php esc_html_e( 'Field type', 'yith-products-addons' ); ?></label>
				<div class="yith-pa-input-container">
					<select id="yith-pa-field-type-<?php echo $loop; ?>-{{INDEX}}" class="yith-pa-field__input yith-pa-field-type"
					name="yith_pa-addon[<?php echo $loop; ?>][{{INDEX}}][field_type]">
						<?php foreach ( $field_types as $field_type ) : ?>
						<option value="<?php echo esc_html( $field_type ); ?>" <?php echo selected( isset( $addon[ $loop ]['field_type'] ) && $addon[ $loop ]['field_type'] === $field_type ); ?>><?php echo esc_html( $field_type ); ?>
						<?php endforeach; ?>
					</select>
					<div class="yith-pa-input-desc"><?php esc_html_e( 'Choose the type of the Add-on field', 'yith-products-addons' ); ?></div>
				</div>
			</div>
			<!-- RADIO FIELD -->
			<div class="yith-pa-field-container">
				<label for="yith-pa-desc-<?php echo $loop; ?>-{{INDEX}}" class="yith-pa-field__label">
				<?php esc_html_e( 'Price Settings', 'yith-products-addons' ); ?></label>
				<div class="yith-pa-input-container">
					<div class="yith-pa-radio-group">
							<!-- FREE PRICE -->
							<span class="yith-pa-single-radio-container yith-pa-field__radio-input">
								<label>
									<input type="radio" value="free" id="yith-pa-radio-free-<?php echo $loop; ?>-{{INDEX}}" class="yith-pa-radio__input yith-pa-radio-free"
									name="yith_pa-addon[<?php echo $loop; ?>][{{INDEX}}][price_settings]"  <?php checked( isset( $addon[ $loop ]['price_settings'] ) && 'free' === $addon[ $loop ]['price_settings'] ); ?>>
									<?php esc_attr_e( 'Free', 'yith-products-addons' ); ?>
								</label>
							</span>
							<!-- FIXED PRICE -->
							<span class="yith-pa-single-radio-container yith-pa-field__radio-input">
								<label>
									<input type="radio" value="fixed" id="yith-pa-radio-fixed" class="yith-pa-radio__input yith-pa-radio-fixed"
									name="yith_pa-addon[<?php echo $loop; ?>][{{INDEX}}][price_settings]"  <?php checked( isset( $addon[ $loop ]['price_settings'] ) && 'fixed' === $addon[ $loop ]['price_settings'] ); ?>>
									<?php esc_attr_e( 'Fixed', 'yith-products-addons' ); ?>
								</label>
							</span>
							<!-- PRICE PER CHARACTER -->
							<span class="yith-pa-single-radio-container yith-pa-field__radio-input">
								<label>
									<input type="radio" value="price-per-char" id="yith-pa-radio-price-per-char" class="yith-pa-radio__input yith-pa-radio-price-per-char"
									name="yith_pa-addon[<?php echo $loop; ?>][{{INDEX}}][price_settings]"  <?php checked( isset( $addon[ $loop ]['price_settings'] ) && 'price-per-char' === $addon[ $loop ]['price_settings'] ); ?>>
									<?php esc_attr_e( 'Price per character', 'yith-products-addons' ); ?>
								</label>
							</span>
					</div>
					<div class="yith-pa-input-desc"><?php esc_html_e( 'Choose how the Add-on influence the product price', 'yith-products-addons' ); ?></div>
				</div>
			</div>
			<!-- PRICE FIELD -->
			<div class="yith-pa-field-container">
				<label for="yith-pa-price-<?php echo $loop; ?>-{{INDEX}}" class="yith-pa-field__label">
				<?php esc_html_e( 'Price', 'yith-products-addons' ); ?></label>
				<div class="yith-pa-input-container">
					<input type="text" id="yith-pa-price-<?php echo $loop; ?>-{{INDEX}}" class="yith-pa-field__input yith-pa-price" min='0'
					name="yith_pa-addon[<?php echo $loop; ?>][{{INDEX}}][price]"
					value="<?php echo esc_html( isset( $addon[ $loop ]['price'] ) ? $addon[ $loop ]['price'] : '' ); ?>">
					<div class="yith-pa-input-desc"><?php esc_html_e( 'The price of the add-on', 'yith-products-addons' ); ?></div>
				</div>
			</div>
			<!-- FREE CHARACTER FIELD -->
			<div class="yith-pa-field-container">
				<label for="yith-pa-free-char-<?php echo $loop; ?>-{{INDEX}}" class="yith-pa-field__label">
				<?php esc_html_e( 'Free Characters', 'yith-products-addons' ); ?></label>
				<div class="yith-pa-input-container">
					<input type="number" id="yith-pa-free-char-<?php echo $loop; ?>-{{INDEX}}" class="yith-pa-field__input yith-pa-free-char" min='0'
					name="yith_pa-addon[<?php echo $loop; ?>][{{INDEX}}][free-char]"
					value="<?php echo esc_html( isset( $addon[ $loop ]['free-char'] ) ? $addon[ $loop ]['free-char'] : '' ); ?>">
					<div class="yith-pa-input-desc"><?php esc_html_e( 'Choose how many free characters', 'yith-products-addons' ); ?></div>
				</div>
			</div>
			<!-- CHECKBOX FIELD -->
			<div class="yith-pa-field-container">
				<label for="yith-pa-def-ena--<?php echo $loop; ?>-{{INDEX}}" class="yith-pa-field__label">
				<?php esc_html_e( 'Default Enabled', 'yith-products-addons' ); ?></label>
				<div class="yith-pa-input-container">
					<label class="yith-pa-switch">
						<input type="checkbox" class="yith-pa-field__input yith-pa-def-ena" value='no' name="yith_pa-addon[<?php echo $loop; ?>][{{INDEX}}][def_ena]"
						id="yith-pa-addon-def-ena-<?php echo $loop; ?>-{{INDEX}}" <?php checked( isset( $addon[ $loop ]['def_ena'] ) && 'yes' === $addon[ $loop ]['def_ena'] ); ?>>
						<span class="slider round"></span>
					</label>	
					<div class="yith-pa-input-desc"><?php esc_html_e( 'Enable if the field is enabled by default', 'yith-products-addons' ); ?></div>
				</div>
			</div>
			<!-- ADD OPTIONS FIELD -->
			<div class="yith-pa-field-container">
				<label for="yith-pa-options-<?php echo $loop; ?>-{{INDEX}}" class="yith-pa-field__label">
				<?php esc_html_e( 'Options', 'yith-products-addons' ); ?></label>
				<div class="yith-pa-input-container">
						<div class="yith-pa-options-block">	
							<div class="yith-pa-options-container__template">
								<div class="yith-pa-single-option-container">
									<p class="yith-pa-option-label"><?php esc_html_e( 'Name', 'yith-products-addons' ); ?></p>
									<input type="text" class="yith-pa-select-option__text-input" name="yith_pa-addon[<?php echo $loop; ?>][{{INDEX}}][options][text][]">
								</div>
								<div class="yith-pa-single-option-container">
									<p class="yith-pa-option-label"><?php esc_html_e( 'Price', 'yith-products-addons' ); ?></p>
									<input type="number" class="yith-pa-select-option__price-input" name="yith_pa-addon[<?php echo $loop; ?>][{{INDEX}}][options][price][]">
								</div>
								<span class="yith-pa-remove-option" id="yith-pa-remove-option">&#128465;</span>
							</div>
								<?php foreach ( $addon[ $loop ]['options'] as $option ) : ?>
										<div class="yith-pa-options-container">
											<div class="yith-pa-single-option-container">
												<p class="yith-pa-option-label"><?php esc_html_e( 'Name', 'yith-products-addons' ); ?></p>
												<input type="text" class="yith-pa-select-option__text-input" name="yith_pa-addon[<?php echo $loop; ?>][{{INDEX}}][options][text][]"
												value="<?php echo esc_html( $option['name'] ); ?>">
											</div>
											<div class="yith-pa-single-option-container">
												<p class="yith-pa-option-label"><?php esc_html_e( 'Price', 'yith-products-addons' ); ?></p>
												<input type="number" class="yith-pa-select-option__price-input" name="yith_pa-addon[<?php echo $loop; ?>][{{INDEX}}][options][price][]"
												value="<?php echo esc_html( $option['price'] ); ?>">
											</div>
											<span class="yith-pa-remove-option dashicons dashicons-trash" id="yith-pa-remove-option"></span>
										</div>
								<?php endforeach; ?>
						</div>
					<span class="yith-pa-add-new-option" id="yith-pa-add-new-option"><?php esc_html_e( 'Add New Option', 'yith-products-addons' ); ?></span>
					<div class="yith-pa-input-desc"><?php esc_html_e( 'Set the options for the Add-on', 'yith-products-addons' ); ?></div>
				</div>
			</div>
			<!-- REMOVE ADD-ON -->
			<span class="yith-pa-remove-addon" id="yith-pa-remove-addon"><?php esc_html_e( 'Remove Add-on', 'yith-products-addons' ); ?></span>
		</div>
	</div>
</div>
