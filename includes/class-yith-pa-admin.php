<?php
/**
 * This file belongs to the YITH Products Addons.
 *
 * This source file is subject to the GNU GENERAL PUBLIC LICENSE (GPL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.gnu.org/licenses/gpl-3.0.txt
 **/ //phpcs:ignore
if ( ! defined( 'YITH_PA_VERSION' ) ) {
	exit( 'Direct access forbidden.' );
}

if ( ! class_exists( 'YITH_PA_Admin' ) ) {

	/**
	 * YITH_PA_Admin
	 */
	class YITH_PA_Admin {

		/**
		 * Main Instance
		 *
		 * @var YITH_PA_Admin
		 * @since 1.0
		 * @access private
		 */

		private static $instance;

		/** //phpcs:ignore
		 *
		 * @return YITH_PA_Admin Main instance
		 * @author Héctor García
		 */
		public static function get_instance() {
			return ! is_null( self::$instance ) ? self::$instance : self::$instance = new self();
		}

		/**
		 * YITH_PA_Admin constructor.
		 */
		private function __construct() {
			// Enqueue.
			add_action( 'admin_enqueue_scripts', array( $this, 'enqueue_scripts' ) );
			// Dequeue.
			add_action( 'wp_enqueue_scripts', array( $this, 'conditionally_load_woc_js_css' ) );
		}
		/**
		 * Enqueue scripts and styles on backend
		 *
		 * @return void
		 */
		public function enqueue_scripts() {
			wp_register_style( 'yith-pa-backend-css', YITH_PA_DIR_ASSETS_CSS_URL . '/yith-pa-backend.css', array(), YITH_PA_VERSION );
			wp_register_script( 'yith-pa-backend-js', YITH_PA_DIR_ASSETS_JS_URL . '/yith-pa-backend.js', array(), YITH_PA_VERSION );
			wp_enqueue_style( 'yith-pa-backend-css' );
			wp_enqueue_script( 'yith-pa-backend-js' );
			wp_enqueue_script( 'jquery-ui-draggable' );
			wp_register_script( 'wc-enhanced-select', WC()->plugin_url() . '/assets/js/admin/wc-enhanced-select.js', array( 'jquery', 'selectWoo' ), $version );
			wp_enqueue_script( 'wc-enhanced-select' );
		}
		/**
		 * Conditionally load woocommerce js and css
		 *
		 * @return void
		 */
		public function conditionally_load_woc_js_css() {
			if ( function_exists( 'is_woocommerce' ) ) {
					// Only load CSS and JS on Woocommerce pages.
				if ( ! is_woocommerce() && ! is_cart() && ! is_checkout() ) {

					// Dequeue scripts.
					wp_dequeue_script( 'woocommerce' );
					wp_dequeue_script( 'wc-add-to-cart' );
					wp_dequeue_script( 'wc-cart-fragments' );

					// Dequeue styles.
					wp_dequeue_style( 'woocommerce-general' );
					wp_dequeue_style( 'woocommerce-layout' );
					wp_dequeue_style( 'woocommerce-smallscreen' );
					wp_dequeue_style( 'wc-block-style' );
					wp_dequeue_style( 'wc-block-vendors-style' );
				}
			}
		}
	}
}

