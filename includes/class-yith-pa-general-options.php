<?php
/**
 * This file belongs to the YITH Products Addons.
 *
 * This source file is subject to the GNU GENERAL PUBLIC LICENSE (GPL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.gnu.org/licenses/gpl-3.0.txt
 */

if ( ! defined( 'YITH_PA_VERSION' ) ) {
	exit( 'Direct access forbidden.' );
}

if ( ! class_exists( 'YITH_PA_Grl_Opt' ) ) {

	/**
	 * YITH_PA_Grl_Opt
	 */
	class YITH_PA_Grl_Opt {

		/**
		 * Main Instance
		 *
		 * @var YITH_PA_Grl_Opt
		 * @since 1.0
		 * @access private
		 */

		private static $instance;

		/**
		 * Main plugin Instance
		 *
		 * @return YITH_PA_Grl_Opt Main instance
		 * @author Héctor García
		 */
		public static function get_instance() {
			return ! is_null( self::$instance ) ? self::$instance : self::$instance = new self();
		}

		/**
		 * YITH_PA_Grl_Opt constructor.
		 */
		private function __construct() {

			add_action( 'admin_menu', array( $this, 'yith_create_pa_custom_menu' ) );
			add_action( 'admin_init', array( $this, 'yith_pa_register_settings' ) );

		}

		/**
		 * Creates the custom menu
		 *
		 * @return void
		 */
		public function yith_create_pa_custom_menu() {

			add_menu_page(
				esc_html__( 'Add-ons', 'yith-products-addons' ),
				esc_html__( 'General Add-ons configuration', 'yith-products-addons' ),
				'manage_options',
				'yith-pa-custom-options',
				array( $this, 'yith_pa_custom_menu_page' ),
				'',
				40
			);

		}

		/**
		 * Get the view to print the custom settings
		 *
		 * @return void
		 */
		public function yith_pa_custom_menu_page() {

			yith_pa_get_view( '/yith-pa-options-panel.php', array() );

		}

		/**
		 * Register the custom settings
		 *
		 * @return void
		 */
		public function yith_pa_register_settings() {

			$page_name = 'yith-pa-options-page';

				add_settings_section(
					'addons-general-options',
					'Add-ons options',
					array( $this, 'yith_pa_show_options' ),
					$page_name,
				);
			if ( isset( $addons[0]['{{INDEX}}'] ) ) {
				unset( $addons[0]['{{INDEX}}'] );
			}
			register_setting( $page_name, 'yith_pa-addon' );

		}

		/**
		 * Show the options template
		 *
		 * @param  mixed $field_data
		 * @param  mixed $index
		 * @return void
		 */
		public function yith_pa_show_options( $field_data, $index = null ) {

			$addons = get_option( 'yith_pa-addon' );
			$products_id = isset( $addons['product_ids'] ) ? $addons['product_ids'] : array();
			// error_log( print_r( $products_id, true ) );
			$addons = isset( $addons[0] ) ? $addons[0] : array();

			unset( $addons[0]['{{INDEX}}'] );

			yith_pa_get_view( '/addons-container.php', compact( 'addons' ) );
			yith_pa_get_view( '/yith-product-search.php', compact( 'products_id' ) );

		}

		/**
		 * Sort addons options
		 *
		 * @param  mixed $addons Addon array.
		 * @return void
		 */
		public function sort_addons_options( &$addons ) {
			foreach ( $addons as &$addon ) {
				$options = array();
				foreach ( $addon[0]['options']['text'] as $key => $text ) {
					if ( ! ! $text ) {
						$options[] = array(
							'name'  => sanitize_text_field( wp_unslash( $text ) ),
							'price' => $addon[0]['options']['price'][ $key ],
						);
					}
				}
				if ( ! $options ) {
					$options[] = array(
						'name'  => '',
						'price' => '',
					);
				}
				$addon[0]['options'] = $options;
			}
		}

	}
}
